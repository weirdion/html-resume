.PHONY: default init

default: init

# Create a job that initializes submodules
init:
	git submodule init && git submodule update --recursive
